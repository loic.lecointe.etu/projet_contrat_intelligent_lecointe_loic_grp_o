# Smart Contract de Vote avec Fonctionnalité de Vote Quadratique

## Auteur
- Loïc LECOINTE
- Membre du groupe O

## Description
Ce dépôt GitLab contient un contrat intelligent (smart contract) en Solidity pour la gestion d'un processus de vote au sein d'une petite organisation. 
Je n'ai pas réussi à réaliser la partie Dapp, cependant voici le smart contrat et la partie DWYW complétement développée. 

Le contrat intelligent permet aux utilisateurs inscrits sur une liste blanche (whitelist) de proposer des idées et de voter sur ces propositions. Les principales caractéristiques et fonctionnalités du contrat sont les suivantes :

- Les utilisateurs inscrits peuvent proposer des idées pendant la session d'enregistrement des propositions.
- Les votes ne sont pas confidentiels pour les utilisateurs ajoutés à la liste blanche, chaque votant peut consulter les votes des autres.
- Le vainqueur est déterminé par une majorité simple, c'est-à-dire que la proposition qui reçoit le plus de votes l'emporte.
- Le processus de vote est géré par un ensemble d'états définis dans une énumération, permettant un suivi clair du déroulement du vote.
- Le contrat utilise la bibliothèque OpenZeppelin "Ownable" pour la gestion de l'administrateur du vote.
- L'ajout de la fonctionnalité de vote quadratique permet aux participants ayant une préférence marquée pour une décision de recevoir des votes supplémentaires, avec un coût quadratique pour chaque vote supplémentaire.

## Fonctionnalités Clés
Le contrat intelligent "Voting" comprend les éléments suivants :

- Structures de données pour les votants et les propositions.
- Énumération pour suivre les états du processus de vote.
- Fonctions pour l'enregistrement des votants, la proposition d'idées, le vote, le calcul du gagnant, etc.
- Événements pour notifier les changements d'état et les actions des votants.

La fonctionnalité de vote quadratique permet aux votants d'acquérir des votes supplémentaires en augmentant le coût de manière quadratique pour chaque vote supplémentaire. Cela favorise une meilleure répartition du pouvoir de vote et évite que de petits groupes puissent prendre des décisions simplement en ayant plus de votants.

## Utilisation
Le dépôt contient le contrat intelligent en Solidity.

1. Clonez ce dépôt GitHub localement.
2. Compilez le contrat en utilisant le compilateur Solidity.
3. Déployez le contrat sur la blockchain Ethereum en utilisant un portefeuille compatible avec Ethereum.
4. Inscrivez les votants sur la liste blanche en tant qu'administrateur.
5. Démarrez les sessions d'enregistrement des propositions et de vote selon le processus souhaité.
6. Les votants peuvent soumettre des idées et voter en conséquence.
7. Le contrat déterminera le gagnant à la fin de la session de vote.

## Contribution
Les contributions à ce projet sont les bienvenues. Si vous souhaitez améliorer le contrat ou ajouter de nouvelles fonctionnalités, n'hésitez pas à ouvrir une demande d'extraction (Pull Request).
